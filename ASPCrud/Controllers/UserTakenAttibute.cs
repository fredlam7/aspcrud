﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ASPCrud.Models.viewModels;
using ASPCrud.Models;
namespace ASPCrud.Controllers
{
    public class UserTakenAttributes : ValidationAttribute
    {
        UsuariosEntities1 db = new UsuariosEntities1();
        public override bool IsValid(object value)
        {
            String user = value.ToString();
            return !db.Usuarios.Any(x => x.usuario == user );            
        }
    }
}