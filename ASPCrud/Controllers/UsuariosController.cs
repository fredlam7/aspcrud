﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ASPCrud.Models;
using ASPCrud.Models.viewModels;

namespace ASPCrud.Controllers
{
    public class UsuariosController : Controller
    {
        // GET: Usuarios
        public ActionResult Index()
        {
            LoginViewModel model = new LoginViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Index(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (UsuariosEntities1 db = new UsuariosEntities1()) {

                        var us = db.Usuarios.Where(u => u.usuario == model.usuario && u.password == model.password).FirstOrDefault();
                        if(us != null) return Redirect("~/Usuarios/menu");
                    }
                }
                return View();
            }
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult menu()
        {
            List<ListUsuariosViewModel> list;
            using (UsuariosEntities1 db = new UsuariosEntities1())
            {
                list = (from d in db.Usuarios
                        select new ListUsuariosViewModel
                        {
                            idUsuario = d.idUsuario,
                            usuario = d.usuario
                        }).ToList();
            }
            return View(list);
        }
       
        public ActionResult NuevoUsuario()
        {
            return View();
        }

        [HttpPost]
       public ActionResult NuevoUsuario(UsuariosViewModel u)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                    using (UsuariosEntities1 db = new UsuariosEntities1())
                    {
                        var user = new Usuarios();
                        user.usuario = u.usuario;
                        user.password = u.password;
                        db.Usuarios.Add(user);
                        db.SaveChanges();
                    }
                    return Redirect("~/Usuarios/menu");

                }
                return View(u);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }

        public ActionResult Editar(int id)
        {
            EditUsuariosViewModel model = new EditUsuariosViewModel();
            using (UsuariosEntities1 db = new UsuariosEntities1())
            {
                var oUsuario = db.Usuarios.Find(id);
                model.usuario = oUsuario.usuario;
                model.idUsuario = oUsuario.idUsuario;
                System.Diagnostics.Debug.WriteLine(model.idUsuario + "usuario");
            }
           return View(model);
        }

        [HttpPost]

        public ActionResult Editar(EditUsuariosViewModel model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    System.Diagnostics.Debug.WriteLine(model.idUsuario + "usuario");
                    using (UsuariosEntities1 db = new UsuariosEntities1())
                    {
                        var user = db.Usuarios.Find(model.idUsuario);
                        if (user != null)
                        {
                            user.usuario = model.usuario;
                            db.SaveChanges();
                        }
                        else return View(model);
                    }
                    return Redirect("~/Usuarios/menu");

                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public ActionResult Eliminar(int id)
        {
            UsuariosViewModel model = new UsuariosViewModel();
            using (UsuariosEntities1 db = new UsuariosEntities1())
            {
                var oUsuarios = db.Usuarios.Find(id);
                db.Usuarios.Remove(oUsuarios);
                db.SaveChanges();
            }
            return Redirect("~/Usuarios/menu");
        }
    }
}