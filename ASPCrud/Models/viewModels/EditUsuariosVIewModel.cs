﻿using ASPCrud.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASPCrud.Models.viewModels
{
    public class EditUsuariosViewModel
    {
        [Required]
        [Display(Name = "idUsuario")]
        public int idUsuario { set; get; }


        [Required]
        [Display(Name = "Usuario")]
        [UserTakenAttributes(ErrorMessage = "Usuario ya registrado")]
        public string usuario { set; get; }

    }
}