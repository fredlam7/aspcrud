﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ASPCrud.Controllers;

namespace ASPCrud.Models.viewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Usuario")]     
        public string usuario { set; get; }

        [Required]
        [Display(Name = "Contraseña")]
        public string password { set; get; }
    }
}